//
//  ReceiptTableViewCell.h
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiptInfo.h"

#define kNOTIFICATION_RETRY_DOWNLOAD	@"RetryDownload"


///////////////////////////////////////////////////////////
@interface ReceiptTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel		*labelName;
@property (nonatomic, retain) IBOutlet UILabel		*labelDate;
@property (nonatomic, retain) IBOutlet UILabel		*labelAmount;
@property (nonatomic, retain) IBOutlet UIButton		*buttonRetry;

- (void)setLabels:(ReceiptInfo *)receiptInfo;

@end
