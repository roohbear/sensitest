//
//  ReceiptInfo.m
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import "ReceiptInfo.h"


////////////////////////////////////////////
@implementation ReceiptInfo

- (id)initWithReceiptName:(NSString *)name amount:(NSString *)amount date:(NSString *)date
{
	self = [super init];
	if(self) {
		self.strName = name;
		self.strAmount = amount;
		self.strDate = date;
	}
	return self;
}

@end
