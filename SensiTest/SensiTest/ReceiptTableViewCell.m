//
//  ReceiptTableViewCell.m
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import "ReceiptTableViewCell.h"


////////////////////////////////////////////////////////////
@implementation ReceiptTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// called when user clicks the Retry button to retry downloading receipts
- (IBAction)buttonRetryClicked:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_RETRY_DOWNLOAD object:nil];
}

- (void)setLabels:(ReceiptInfo *)receiptInfo
{
	self.labelName.text = receiptInfo.strName;
	self.labelDate.text = receiptInfo.strDate;
	self.labelAmount.text = receiptInfo.strAmount;
	
	// if date and amount are empty, then we're probably just displaying a 'retry' message so show the 'retry' button
	if(receiptInfo.strDate.length == 0 && receiptInfo.strAmount.length == 0) {
		self.buttonRetry.hidden = NO;
		self.buttonRetry.backgroundColor = [UIColor whiteColor];
		self.buttonRetry.layer.borderWidth = 1;
		self.buttonRetry.layer.borderColor = [[UIColor blackColor] CGColor];
		self.buttonRetry.layer.cornerRadius = 10.0;
	}else{
		self.buttonRetry.hidden = YES;
	}
}

@end
