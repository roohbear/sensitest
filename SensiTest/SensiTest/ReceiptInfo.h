//
//  ReceiptInfo.h
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import <Foundation/Foundation.h>


//////////////////////////////////////////////
@interface ReceiptInfo : NSObject

@property (nonatomic, retain) NSString		*strName;
@property (nonatomic, retain) NSString 		*strDate;
@property (nonatomic, retain) NSString		*strAmount;

- (id)initWithReceiptName:(NSString *)name amount:(NSString *)amount date:(NSString *)date;

@end
