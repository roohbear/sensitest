//
//  main.m
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
