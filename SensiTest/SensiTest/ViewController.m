//
//  ViewController.m
//  SensiTest
//
//  Created by Greg on 2016-03-13.
//  Copyright © 2016 Rooh Bear corp. All rights reserved.
//

#import "ViewController.h"
#import "ReceiptTableViewCell.h"
#import "ReceiptInfo.h"


//////////////////////////////////////////////
@interface ViewController ()

@property (nonatomic, retain) IBOutlet UITableView				*tableviewReceipts;

// "Please Wait dialog" UI bits
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView	*activityIndicator;
@property (nonatomic, retain) IBOutlet UIView					*viewPleaseWait;
@property (nonatomic, retain) IBOutlet UILabel					*labelPleaseWaitMessage;
@property (nonatomic, retain) IBOutlet UIButton					*buttonPleaseWaitOK;

// an array of ReceiptInfo objects (used for the table)
@property (nonatomic, retain) NSArray							*arrReceipts;

@end


//////////////////////////////////////////////
@implementation ViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.title = @"Receipts";

	// listen for if the user wants to retry the download
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRetryDownload:) name:kNOTIFICATION_RETRY_DOWNLOAD object:nil];
	
	[self startDownload];
}

// called when we hear the kNOTIFICATION_RETRY_DOWNLOAD notification
- (void)doRetryDownload:(NSNotification *)n
{
	[self startDownload];
}

// called on main thread to start the downloading process
- (void)startDownload
{
	// show the 'please wait' dialog
	self.viewPleaseWait.hidden = NO;
	self.viewPleaseWait.layer.borderColor = [[UIColor blackColor] CGColor];
	self.viewPleaseWait.layer.cornerRadius = 10.0;
	self.viewPleaseWait.layer.borderWidth = 2.0;
	
	// download the list of receipts in the background
	[self performSelectorInBackground:@selector(downloadReceipts) withObject:nil];
}

// called when "OK" button in Please Wait dialog is clicked
- (IBAction)buttonPleaseWaitOKClicked:(id)sender
{
	self.viewPleaseWait.hidden = YES;
}

#pragma mark - downloading receipts stuff

// Called on background thread to start the downloading of receipts.
// Also parses the result and sorts it
- (void)downloadReceipts
{
	NSError 	*err = nil;
	NSURL 		*url = [NSURL URLWithString:@"https://getsensibill.com/api/tests/receipts"];
	NSData		*dataRaw = [[NSData alloc] initWithContentsOfURL:url];

	if(dataRaw.length > 0) {
		NSDictionary *dictReceipts = [NSJSONSerialization JSONObjectWithData:dataRaw options:NSJSONReadingMutableLeaves error:&err];

		// make a temporary arry of unsorted receipt objects
		NSMutableArray *arrUnsortedReceipts = [[NSMutableArray alloc] init];

		// copy from dictReipts to self.arrReceipts
		NSArray *arrAllReceipts = [dictReceipts objectForKey:@"receipts"];
		for(NSDictionary *dictReceipt in arrAllReceipts) {
			NSDictionary *dictDisplay = [dictReceipt objectForKey:@"display"];
			
			ReceiptInfo *receiptInfo = [[ReceiptInfo alloc] initWithReceiptName:dictDisplay[@"name"] amount:dictDisplay[@"amount"] date:dictDisplay[@"date"]];
			[arrUnsortedReceipts addObject:receiptInfo];
		}

		// sort from arrUnsortedReceipts into self.arrReceipts
		self.arrReceipts = [arrUnsortedReceipts sortedArrayWithOptions:NSSortConcurrent
													usingComparator:^(ReceiptInfo *receipt1, ReceiptInfo *receipt2)
																	  {
																		  return [receipt1.strName compare:receipt2.strName];
																	  }];
		NSLog(@"A");
		[self performSelectorOnMainThread:@selector(downloadReceiptsDone:) withObject:nil waitUntilDone:NO];
	}else{
		[self performSelectorOnMainThread:@selector(downloadReceiptsDone:) withObject:@"Error downloading" waitUntilDone:NO];
	}

}

// Called on the main thread when the receipts are done downloading..
// Refreshes the table and removes the Please Wait dialog.
// if strErrorMessage is nil, then there was no error otherwise display it in the Please Wait dialog
// and show the OK button to dismiss it
- (void)downloadReceiptsDone:(NSString *)strErrorMessage
{
	if(strErrorMessage) {
		// got an error! hide the spinner and show the OK button to dismiss the dialog
		self.activityIndicator.hidden = YES;
		self.labelPleaseWaitMessage.text = strErrorMessage;
		self.buttonPleaseWaitOK.hidden = NO;
		self.buttonPleaseWaitOK.backgroundColor = [UIColor whiteColor];
		self.buttonPleaseWaitOK.layer.borderColor = [[UIColor darkGrayColor] CGColor];
		self.buttonPleaseWaitOK.layer.borderWidth = 1;
		self.buttonPleaseWaitOK.layer.cornerRadius = 10.0;

		ReceiptInfo *receiptFake = [[ReceiptInfo alloc] initWithReceiptName:@"Error downloading receipts" amount:@"" date:@""];
		self.arrReceipts = [[NSMutableArray alloc] initWithObjects:receiptFake, nil];
	}else{
		// no error! hide the dialog and refresh the table
		self.viewPleaseWait.hidden = YES;
	}

	[self.tableviewReceipts reloadData];
}

#pragma mark - UITableView datasource and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.arrReceipts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ReceiptTableViewCell *ret = [tableView dequeueReusableCellWithIdentifier:@"gregCell"];
	if(!ret) {
		ret = [[ReceiptTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"gregCell"];
	}

	ReceiptInfo *receipt = self.arrReceipts[indexPath.row];
	[ret setLabels:receipt];
	
	return ret;
}

@end
